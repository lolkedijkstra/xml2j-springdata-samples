package com.xml2j.discogs.releases.repo;

import com.xml2j.discogs.releases.ReleaseType;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ReleasesTypeRepo extends MongoRepository<ReleaseType, Long> {}
