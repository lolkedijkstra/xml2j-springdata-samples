package com.xml2j.discogs.releases.processor;

import com.xml2j.discogs.releases.repo.ReleasesTypeRepo;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;

public class ReleasesRepo {
    final String path = new ClassPathResource("spring-config.xml").getPath();
    ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(path);
    ReleasesTypeRepo repo = context.getBean(ReleasesTypeRepo.class);

    public void close() {
        if (context != null )
            context.close();
    }

    public ReleasesTypeRepo getRepo() {
        return repo;
    }
}
