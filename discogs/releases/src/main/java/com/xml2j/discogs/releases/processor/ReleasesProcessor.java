package com.xml2j.discogs.releases.processor;


/******************************************************************************
  -----------------------------------------------------------------------------
  XML2J XSD to Java code generator
  -----------------------------------------------------------------------------
  
  This code was generated using XML2J code generator.
  
  Version: 2.5.1 
  Project home: XML2J https://github.com/lolkedijkstra/ 

  Module: RELEASES 
  Generation date: Mon Sep 07 15:48:30 CEST 2020 
  Author: XML2J-GEN

******************************************************************************/
	
import com.xml2j.discogs.releases.ReleaseType;
import com.xml2j.discogs.releases.repo.ReleasesTypeRepo;
import com.xml2j.xml.core.ComplexDataType;
import com.xml2j.xml.core.MessageProcessor;
import com.xml2j.xml.core.ProcessorException;
import com.xml2j.xml.core.XMLEvent;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;

/**
 *	This class processes events that are sent by the XML2J framework.
 */
public class ReleasesProcessor implements MessageProcessor {

	private ReleasesRepo releasesRepo = new ReleasesRepo();

	public ReleasesRepo getReleasesRepo() {
		return releasesRepo;
	}

	@Override
	public void process(XMLEvent evt, ComplexDataType data)
			throws ProcessorException {

		if (evt == XMLEvent.END && data instanceof ReleaseType) {
			process((ReleaseType)data);
		}
	}

	private void process(ReleaseType data) {
//		System.out.println( data.getClass().toString() );
		try {
			data.setDiscogsId(Long.parseLong(data.getAttributes().get("id")));
		} catch (NumberFormatException e) {}
		releasesRepo.getRepo().save(data);
	}
}
